import assert from 'assert';
// @ts-ignore
import { Flake } from '../source';

describe('flake', () => {
  it('make zero', () => {
    const size = 0;
    const list = Flake.makeList(size);
    assert.strictEqual(size, list.length);
  });

  it('make one', () => {
    const item = Flake.makeOne();

    // noinspection MagicNumberJS
    assert.strictEqual(39, item.length);
  });

  it('make list', () => {
    const size = 10;
    const list = Flake.makeList(size);
    // console.dir(list);
    assert.strictEqual(size, list.length);
  });
});
