import os from 'os';
import process from 'process';

export const defaultMacAddress = '00:00:00:00:00:00';

export function getNetworkInterfaces(withDefaultMacAddress: boolean = false): os.NetworkInterfaceInfo[] {
  const networkInterfaces = os.networkInterfaces();
  const weight = { internal: 0b10, family: 0b01 };
  let items: os.NetworkInterfaceInfo[] = [];

  for (let name in networkInterfaces) {
    items = items.concat(networkInterfaces[name]);
  }

  if (!withDefaultMacAddress) {
    items = items.filter(item => defaultMacAddress != item.mac);
  }

  return items
    .map((item, index) => {
      let value = 0;

      if (!item.internal) {
        value += weight.internal;
      }

      if ('IPv6' === item.family) {
        value += weight.family;
      }

      return { index, value };
    })
    .sort((left, right) => right.value - left.value)
    .map(item => items[item.index]);
}

export function padZeroHex(item: number, length: number): string {
  const text = '0'.repeat(length) + item.toString(16);
  return text.slice(-length);
}

export class Flake {
  static readonly instance = new Flake();

  protected readonly address = this.getMacAddress();
  protected readonly bytes = { timestamp: 14, count: 4, process: 6, mac: 12 };
  protected count = 0;
  protected readonly max = { count: 0xffff };
  protected readonly process = this.getProcess();
  protected timestamp = Date.now();

  protected constructor() {}

  static makeList(total: number): string[] {
    if (0 >= total) {
      return [];
    }

    const items: any = {};
    let count = total;

    while (0 < count) {
      const item = Flake.makeOne();

      if (!items.hasOwnProperty(item)) {
        items[item] = true;
        count--;
      }
    }

    return Object.keys(items);
  }

  static makeOne(): string {
    return Flake.instance.make();
  }

  make(): string {
    const timestamp = this.makeTimestamp();
    const count = this.makeCount();
    return `${timestamp}-${count}-${this.process}-${this.address}`.toLowerCase();
  }

  protected getMacAddress(): string {
    const items = getNetworkInterfaces();
    const item = 0 === items.length ? defaultMacAddress : items[0].mac;
    return item.replace(/:/g, '');
  }

  protected getProcess(): string {
    return padZeroHex(process.pid, this.bytes.process);
  }

  protected makeCount(): string {
    const count = padZeroHex(this.count, this.bytes.count);
    this.count++;

    if (this.max.count < this.count) {
      this.count = 0;
    }

    return count;
  }

  protected makeTimestamp(): string {
    const current = Date.now();

    if (current > this.timestamp) {
      this.count = 0;
      this.timestamp = current;
    }

    return padZeroHex(current, this.bytes.timestamp);
  }
}

export const flake = Flake.instance;
